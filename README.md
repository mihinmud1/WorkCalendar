# WorkCalendar
This project was made as a solution to a huge problem I always had at school. Whenever I or one of my friends was absent, it was always tiring to request the work that we'd missed. Sometimes I would write the wrong notes in the wrong book, or maybe mix up the whole subject altogether! This website solves that problem by making it easy for everyone to upload work to one server and keeping it organized by subject and date. This was completely made for fun, and I can't guarantee its security. I only made this public as a contribution to open source, so anyone can learn from my experience using PocketBase and SvelteKit together.

# Steps for localhost
Steps for running the project locally

- Clone the repository.
- Download PocketBase
- Import the `pb_schema.json` into PocketBase.
- Put the PocketBase server URL in the  `POCKETBASE` enviroment variable (see `.env.example`)
- Install dependencies
- Run the svelte project with `npm run dev`.
