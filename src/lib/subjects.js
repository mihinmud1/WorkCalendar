export const subjects = [
	{ value: 'maths', name: 'Mathematics' },
	{ value: 'science', name: 'Science' },
	{ value: 'sinhala', name: 'Sinhala Language' },
	{ value: 'history', name: 'History' },
	{ value: 'commerce', name: 'Commerce' },
	{ value: 'english', name: 'English Language' },
	{ value: 'it', name: 'Information & Communication Technology' },
	{ value: 'health', name: 'Health Science' },
	{ value: 'catholic', name: 'Catholic' },
	{ value: 'buddhism', name: 'Buddhism' },
	{ value: 'english_literature', name: 'English Literature' },
	{ value: 'western_music', name: 'Western Music' },
	{ value: 'eastern_music', name: 'Eastern Music' },
	{ value: 'art', name: 'Art' },
	{ value: 'dancing', name: 'Dancing' },
	{ value: 'drama', name: 'Drama' }
];
