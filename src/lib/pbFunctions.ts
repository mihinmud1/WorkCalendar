import DOMPurify from 'isomorphic-dompurify';

const formatData = (data: FormData) => {
	let formatedData = new FormData();

	//@ts-ignore
	if (!(data.get('files').size <= 0)) {
		for (let file of data.getAll('files')) {
			formatedData.append('files', file);
		}
	}

	//@ts-ignore
	formatedData.append('details', DOMPurify.sanitize(data.get('details')));

	return formatedData;
};

// pass in a pb client object, gradeClass ex:"10s5", subject, date ex: "2023-08-08", and data which is a FormData
// data object might contain details field, files fields and files- fields

export const createRecord = async (
	pb: any,
	gradeClass: string,
	subject: string,
	date: string,
	data: FormData
) => {
	let newRecord = formatData(data);
	newRecord.append('gradeClass', gradeClass);
	newRecord.append('subject', subject);
	newRecord.append('date', date);
	newRecord.append('uploader', pb.authStore.model.id);
	try {
		try {
			await pb
				.collection('files')
				.getFirstListItem(
					`gradeClass = "${gradeClass}" && subject = "${subject}" && date = "${date}" `
				);
			return { success: false, existing: true };
		} catch (e) {
			await pb.collection('files').create(newRecord);
			return { success: true };
		}
	} catch (error) {
		return { sccess: false, error };
	}
};

export const updateWork = async (
	pb: any,
	gradeClass: string,
	subject: string,
	date: string,
	data: FormData
) => {
	let newRecord = formatData(data);
	newRecord.append('editors+', pb.authStore.model.id);
	let removeFiles = data.getAll('files-');

	try {
		const record = await pb
			.collection('files')
			.getFirstListItem(
				`gradeClass = "${gradeClass}" && subject = "${subject}" && date = "${date}" `
			);

		if (record) {
			await pb.collection('files').update(record.id, newRecord);

			if (removeFiles) {
				await pb.collection('files').update(record.id, { 'files-': removeFiles });
			}

			return { success: true };
		} else {
			return { existing: false };
		}
	} catch (e) {
		return { success: false, error: e };
	}
};

export const getSubjectWork = async (
	pb: any,
	grade: any,
	classNum: string,
	subject: string,
	date: string
) => {
	try {
		const info = await pb
			.collection('files')
			.getFirstListItem(
				`gradeClass="${grade}${classNum}" && subject="${subject}" && date="${date}"`,
				{
					expand: 'editors,uploader'
				}
			);

		// formating files into a useful list
		let files = [];

		for (const file of info.files) {
			files.push({
				name: file,
				URL: pb.files.getUrl(info, file)
			});
		}

		// formating editors into a useful objects
		let editors = [];

		for (const editor of info.expand?.editors || []) {
			editors.push({
				username: editor.username
			});
		}

		return {
			files,
			details: info.details,
			uploader: { username: info.expand.uploader.username },
			editors
		};
	} catch (e: any) {
		if (e.response.code == 404) {
			return { error: 404 };
		}
	}
};

export const getDayWorkSubjects = async (pb: any, grade: any, classNum: string, date: string) => {
	let work = await pb
		.collection('files')
		.getList(1, 25, { filter: `gradeClass="${grade}${classNum}" && date="${date}"` });

	let subjects: any = [];
	for (let workOfSubject of work.items) {
		subjects.push(workOfSubject.subject);
	}
	return subjects;
};
