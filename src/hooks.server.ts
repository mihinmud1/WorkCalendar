import PocketBase from 'pocketbase';
import { env } from '$env/dynamic/private';

export const handle = async ({ event, resolve }: { event: any; resolve: any }) => {
	event.locals.pb = new PocketBase(env.POCKETBASE);

	// load the store data from the request cookie string
	event.locals.pb.authStore.loadFromCookie(event.request.headers.get('cookie') || '');

	if (event.locals.pb.authStore.isValid) {
		event.locals.user = event.locals.pb.authStore.model;
	}

	const response = await resolve(event);

	// send back the default 'pb_auth' cookie to the client with the latest store state
	response.headers.append(
		'set-cookie',
		event.locals.pb.authStore.exportToCookie({ secure: false, httpOnly: false, sameSite: false })
	);

	return response;
};
