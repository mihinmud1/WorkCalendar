import { getDayWorkSubjects, getSubjectWork } from '$lib/pbFunctions';

export const load = async ({ locals, params }: any) => {
	let grade = params.grade;
	let classNum = params.classNum;
	let date = params.date;

	return {
		grade,
		classNum,
		date,
		subjects: await getDayWorkSubjects(locals.pb, grade, classNum, date)
	};

};
