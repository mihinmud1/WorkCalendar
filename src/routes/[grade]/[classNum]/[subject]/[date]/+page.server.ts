// @ts-nocheck
import { getSubjectWork, updateWork } from '$lib/pbFunctions';
import { redirect } from '@sveltejs/kit';

export const load = async ({ locals, params }) => {
	let data = {
		grade: params.grade,
		classNum: params.classNum,
		subject: params.subject,
		date: params.date,
		info: await getSubjectWork(
			locals.pb,
			params.grade,
			params.classNum,
			params.subject,
			params.date
		),
		loggedIn: locals.pb.authStore.isValid
	};

	return data;
};

export const actions = {
	default: async ({ request, params, locals }) => {
		const gradeClass = params.grade + params.classNum;
		const data: any = await request.formData();

		const status = await updateWork(locals.pb, gradeClass, params.subject, params.date, data);

		if (status.existing) {
			throw redirect('/upload');
		}

		return { success: status.success };
	}
};
