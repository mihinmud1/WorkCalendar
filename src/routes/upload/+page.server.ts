import { redirect, fail } from '@sveltejs/kit';
import { createRecord } from '$lib/pbFunctions';

export const actions = {
	default: async ({ locals, request }: any) => {
		const data: any = await request.formData();
		const gradeClass = data.get('gradeNum') + data.get('classNum');

		let status = await createRecord(
			locals.pb,
			gradeClass,
			data.get('subject'),
			data.get('date'),
			data
		);

		if (!status.success) {
			if (status.existing) {
				throw redirect(
					303,
					`/${data.get('gradeNum')}/${data.get('classNum')}/${data.get('subject')}/${data.get(
						'date'
					)}`
				);
			}
			console.log(status.error);
			return fail(400);
		}
	}
};
export const load = ({ locals }: any) => {
	if (!locals.pb.authStore.isValid) {
		throw redirect(303, '/account/login');
	}
};
