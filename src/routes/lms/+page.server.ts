import { redirect } from '@sveltejs/kit';

export const load = ({ locals }: any) => {
	throw redirect(303, 'https://mecandycat.github.io/LMS-Dash/');
};
