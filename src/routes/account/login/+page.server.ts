import { redirect } from '@sveltejs/kit';

export const actions = {
	default: async ({ locals, request }: { locals: any; request: Request }) => {
		const data = await request.formData();
		const username: any = data.get('username');
		const password: any = data.get('password');
		try {
			if (username && password) {
				await locals.pb.collection('users').authWithPassword(username, password);
				return { success: true };
			}
		} catch (e) {
			console.log(e);
			return { success: false };
		}
		throw redirect(303, '/account');
	}
};

export const load = ({ locals }: any) => {
	if (locals.pb.authStore.isValid) {
		throw redirect(303, '/account');
	}
};
