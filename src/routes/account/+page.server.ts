import { redirect } from '@sveltejs/kit';
import type { Actions } from '@sveltejs/kit';

export const actions = {
	default: async ({ locals }: any) => {
		locals.pb.authStore.clear();
		locals.user = undefined;
		return { success: true };
	}
} satisfies Actions;

export const load = ({locals} : any) => {
	if (!locals.pb.authStore.isValid) {
		throw redirect(303, '/account/login')
	}
}