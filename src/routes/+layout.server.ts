export const load = ({ locals }: { locals: any }) => {
	if (locals?.user) {
		return { user: { username: locals.user.username } };
	} else {
		return { users: false };
	}
};
